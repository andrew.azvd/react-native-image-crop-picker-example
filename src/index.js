/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, { Component } from 'react';
import { Platform, Text, View, TouchableOpacity, Image } from 'react-native';
import ImagePicker from 'react-native-image-crop-picker';

import styles from './styles';

type Props = {};

type State = {
  image: string;
};

class App extends Component<Props, State> {

  state = {
    image: '',
  };

  openImagePicker = () => {
    ImagePicker.openPicker({
      width: 300,
      height: 400,
      cropping: true
    }).then(image => {
      this.setState({
        image: Platform.OS === 'android' ? image.path : image.sourceURL,
      });
    }).catch(error => {
      console.log('Cancelled!');
    });
  }

  render() {
    const { image } = this.state;

    return (
      <View style={styles.container}>
        <TouchableOpacity style={styles.button} onPress={this.openImagePicker}>
          <Text style={styles.buttonText}>Click me!</Text>
        </TouchableOpacity>

        <Image style={styles.image} source={{ uri: image }} />
      </View>
    );
  }
}

export default App;
