import { StyleSheet } from 'react-native';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    paddingHorizontal: 20,
    backgroundColor: '#379FB1',
  },
  button: {
    alignItems: 'center',
    justifyContent: 'center',
    height: 40,
    width: '50%',
    borderRadius: 3,
    borderWidth: 3,
    borderColor: '#fceac8',
  },
  buttonText: {
    fontSize: 18,
    fontWeight: 'bold',
    color: '#fceac8',
  },
  image: {
    marginTop: 60,
    width: 200,
    height: 200,
    borderWidth: 3,
    borderColor: '#fceac8',
  },
});

export default styles;
